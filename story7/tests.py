from django.test import TestCase, Client
from django.urls import resolve
from .views import story7ku, delete, confirmation
from .models import modelStory7
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

class testStory7(TestCase):

    def test_app_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_landing_page_is_using_correct_html(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'story7.html')

    def test_function_story7ku(self):
        found = resolve('/')
        self.assertEqual(found.func, story7ku)

    def test_function_delete(self):
        found = resolve('/1/')
        self.assertEqual(found.func, delete)
    
    def test_function_confirmation(self):
        found = resolve('/confirmation/')
        self.assertEqual(found.func, confirmation)

    def test_landing_page_is_writtten(self):
        self.assertIsNotNone(story7ku)

    def test_model_check(self):
        modelStory7.objects.create(nama='pepew',status='baik')
        count = modelStory7.objects.all().count()
        self.assertEqual(count, 1)

    def test_form(self):
        response = self.client.post('/', follow=True, data={
            'nama' : 'Pepew',
            'status' : 'Baik'
        })
        self.assertEqual(modelStory7.objects.count(), 1)
    
    def test_form2(self):
        response = self.client.post('/10/', follow=True, data={
            'nama' : 'Pepew',
            'status' : 'Baik'
        })
        self.assertEqual(modelStory7.objects.count(), 1)

    def test_app_url_is_exist2(self):
        response = Client().get('/10/')
        self.assertEqual(response.status_code, 200)

class Story7Test(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Story7Test, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Story7Test, self).tearDown()

    def test_input_form_yes(self):  
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        time.sleep(5)
        namaUser = selenium.find_element_by_name('nama')
        time.sleep(3)
        statusUser = selenium.find_element_by_name('status')
        time.sleep(3)
        submit = selenium.find_element_by_name('submit')


        namaUser.send_keys("Pepew")
        time.sleep(3)
        statusUser.send_keys("Sehat")
        time.sleep(3)

        submit.send_keys(Keys.RETURN)

        selenium.get('http://127.0.0.1:8000/confirmation')
        time.sleep(3)

        submit_confirmation = selenium.find_element_by_name("yes")
        submit_confirmation.send_keys(Keys.RETURN)
        time.sleep(3)

        selenium.get('http://127.0.0.1:8000/')

    def test_input_form_cancel(self):

        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        time.sleep(5)
        namaUser = selenium.find_element_by_name('nama')
        time.sleep(3)
        statusUser = selenium.find_element_by_name('status')
        time.sleep(3)
        submit = selenium.find_element_by_name('submit')


        namaUser.send_keys("Pepew")
        time.sleep(3)
        statusUser.send_keys("Sehat")
        time.sleep(3)

        submit.send_keys(Keys.RETURN)

        selenium.get('http://127.0.0.1:8000/confirmation')
        time.sleep(3)

        submit_confirmation = selenium.find_element_by_name("no")
        submit_confirmation.send_keys(Keys.RETURN)
        time.sleep(3)

        selenium.get('http://127.0.0.1:8000/')
