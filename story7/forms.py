from django import forms

class formStory7(forms.Form):
    nama = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Nama Pengguna',
        'type' : 'text',
        'required' : True
    }))
    status = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Status',
        'type' : 'text',
        'required' : True
    }))
