from django.urls import path
from . import views

appname = 'story7'
urlpatterns = [
    path('', views.story7ku, name='story7ku'),
    path('<int:pk>/', views.delete, name='delete'),
    path('confirmation/', views.confirmation, name='confirmation'),
]
