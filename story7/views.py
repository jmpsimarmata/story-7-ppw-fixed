
from django.shortcuts import render, redirect
from . import forms,models

def story7ku(request):
    if request.method == "POST":
        context = forms.formStory7(request.POST)
        if context.is_valid():
            context1 = models.modelStory7()
            context1.nama = context.cleaned_data["nama"]
            context1.status = context.cleaned_data["status"]
            context1.save()
        return redirect("/confirmation")
    else:
        context = forms.formStory7()
        context1 = models.modelStory7.objects.all()
        context_dict = {
            'form' : context,
            'isi_form' : context1
        }
        return render(request, 'story7.html', context_dict)

def delete(request, pk):
    if request.method == "POST":
        context = forms.formStory7(request.POST)
        if context.is_valid():
            context1 = models.modelStory7()
            context1.nama = context.cleaned_data["nama"]
            context1.status = context.cleaned_data["status"]
            context1.save()
        return redirect("/confirmation")
    else:
        models.modelStory7.objects.filter(pk = pk).delete()
        context = forms.formStory7()
        context1 = models.modelStory7.objects.all()
        context_dict = {
            'form' : context,
            'isi_form' : context1
        }
        return render(request, 'story7.html', context_dict)

def confirmation(request):
    last = models.modelStory7.objects.last()
    context_dict = {
        'isi_form' : last
    }
    return render(request, 'confirmation.html', context_dict)
